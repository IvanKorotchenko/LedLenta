void serialEvent() {
  char c = Serial.read();
  if (c == '\n') {
    Command();
    input_string = "";
    led.setColor(NeoPixelAll, Color[0], Color[2], Color[1], bright);
    led.write();
  }
  else
  {
    input_string += c;
  }
}

void Command()
{
  //Serial.print("Input_string is: ");
  //Serial.println(input_string);
  if (input_string.startsWith("R") == true) // влажность
  {
    input_string.replace("R", "");
    Color[0] = input_string.toInt();      // преобразовываем строку в число
    EEPROM.write(0, Color[0]);
  }
  else if (input_string.startsWith("B") == true) // влажность
  {
    input_string.replace("B", "");
    Color[1] = input_string.toInt();      // преобразовываем строку в число
    EEPROM.write(1, Color[1]);
  }
  else if (input_string.equals("White") == true) // влажность
  {
    for (int i = 0; i < 3; i++) {
      Color[i] = 255;
    }
  }
  else if (input_string.startsWith("G") == true) // аналоговый вход
  {
    input_string.replace("G", "");    // заменяем строку
    Color[2] = input_string.toInt();      // преобразовываем строку в число
    EEPROM.write(2, Color[2]);
  }
  else if (input_string.startsWith("br") == true) // влажность
  {
    input_string.replace("br", "");    // заменяем строку
    bright = input_string.toInt();      // преобразовываем строку в число
  }
}



void led_blink() {
  int h;
  for (int i = 0; i < 21; i++) {
    h += 20;
    led.setColor(i, 0xFFFFFF);
    led.write();
    //Serial.println("ff");
    delay(50);
  }
  delay(2250);
  for (int p = 21; p > 0; p--) {
    led.setColor(p, 0x000000);
    led.write();
    //Serial.println("gg");
    delay(50);
  }
  led.setColor(NeoPixelAll, 0x000000);
  led.write();
}

